<?php

namespace App\Http\Controllers\API;

use App\Models\DataAgenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgendaController extends Controller
{
    public function get() {
        $data = DataAgenda::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = DataAgenda::find($id);
        return response()->json([
            "massage" => "data per id terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'nama_acara' => $request->nama_acara,
            'penyelenggara' => $request->penyelenggara,
            'tempat' => $request->tempat,
            'deskripsi' => $request->deskripsi,
            'tanggal' => $request->tanggal,
            'due_date' => $request->due_date
        ];
        DataAgenda::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'nama_acara' => $request->nama_acara,
            'penyelenggara' => $request->penyelenggara,
            'tempat' => $request->tempat,
            'deskripsi' => $request->deskripsi,
            'tanggal' => $request->tanggal,
            'due_date' => $request->due_date
        ];
        $dataagenda = DataAgenda::find($id);
        $dataagenda->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        DataAgenda::destroy($id);
        return response()->json([
            "massage" => "data berhasial dihapus",
        ]);
    }
}
