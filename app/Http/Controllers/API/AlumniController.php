<?php

namespace App\Http\Controllers\API;

use App\Models\DataAlumni;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlumniController extends Controller
{
    public function get() {
        $data = DataAlumni::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = DataAlumni::find($id);
        return response()->json([
            "massage" => "data per id terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'siswa_id' => $request->siswa_id,
        ];
        DataAlumni::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'siswa_id' => $request->siswa_id,
        ];
        $dataalumni = DataAlumni::find($id);
        $dataalumni->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        DataAlumni::destroy($id);
        return response()->json([
            "massage" => "data berhasil dihapus",
        ]);
    }
}
