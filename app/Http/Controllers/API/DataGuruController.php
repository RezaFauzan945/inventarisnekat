<?php
namespace App\Http\Controllers\API;

use App\Models\DataGuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class DataGuruController extends BaseController
{
    public function get() {
        $data = DataGuru::all();
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = DataGuru::find($id);
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'nip'=>$request->nip,
            'nama'=>$request->nama,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'agama'=>$request->agama,
            'alamat'=>$request->alamat,
            'tempat_lahir'=>$request->tempat_lahir,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'foto'=>$request->foto,
            'status_ptk'=>$request->status_ptk,
            'user_id'=>$request->user_id
        ];
        DataGuru::create($data);
        return response()->json([
            "message" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'nip'=>$request->nip,
            'nama'=>$request->nama,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'agama'=>$request->agama,
            'alamat'=>$request->alamat,
            'tempat_lahir'=>$request->tempat_lahir,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'foto'=>$request->foto,
            'status_ptk'=>$request->status_ptk,
            'user_id'=>$request->user_id
        ];
        DataGuru::find($id)->update($data);
        return response()->json([
            "message" => "data berhasil diedit",
            "data" => $data
        ]);
    }

    public function delete($id) {
        DataGuru::destroy($id);
        return response()->json([
            "message" => "data berhasil dihapus"
        ]);
    }
}
