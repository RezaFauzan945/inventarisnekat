<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kos;

class DataKosController extends Controller
{
    public function get() {
        $data = Kos::all();
        return response()->json([
            "massage" => "data berhasil diambil",
            "data" => $data,
        ]);
    }
    public function getById($id) {
        $data = Kos::find($id);
        return response()->json([
            "massage" => "data berhasil didapat",
            "data" => $data,
        ]);
    }
    public function create(Request $request) {
        $data = Kos::create($request->all());
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data,
        ]);
    }
    public function update(Request $request, $id) {
        $data = Kos::find($id);
        $data->update($request->all());
        return response()->json([
            "massage" => "data berhasil diubah",
            "data" => $data,
        ]);
    }
    public function delete($id) {
        $data = Kos::find($id);
        $data->delete();
        return response()->json([
            "massage" => "data berhasil hapus",
        ]);
    }
}
