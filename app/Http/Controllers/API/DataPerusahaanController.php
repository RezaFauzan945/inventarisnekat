<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\DataJurusan;
use Illuminate\Http\Request;
use App\Models\DataPerusahaan;

class DataPerusahaanController extends Controller
{
    public function get() {
        $data = DataPerusahaan::all();
        return response()->json([
            "massage" => "data berhasil dipangil",
            "data" => $data,
        ]);
    }
    public function getById($id) {
        $data = DataPerusahaan::find($id);
        return response()->json([
            "massage" => "data berhasil didapat",
            "data" => $data,
        ]);
    }
    public function create(Request $request) {
        $data = DataPerusahaan::create($request->all());
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data,
        ]);
    }
    public function update(Request $request, $id) {
        $data = DataPerusahaan::find($id);
        $data->update($request->all());
        return response()->json([
            "massage" => "data berhasil diubah",
            "data" => $data,
        ]);
    }
    public function delete($id) {
        $data = DataPerusahaan::find($id);
        $data->delete();
        return response()->json([
            "massage" => "data berhasil hapus",
        ]);
    }
}
