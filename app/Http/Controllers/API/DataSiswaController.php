<?php

namespace App\Http\Controllers\API;

use App\Models\DataSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class DataSiswaController extends BaseController
{
    public function get(){
        $data = DataSiswa::all();
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id){
        $data = DataSiswa::find($id);
        return response()->json([
            "message"=>"data terambil",
            "data"=>$data
        ]);
    }

    public function create(Request $request){
        $data=[
            'nipd'=>$request->nipd,
            'nisn'=>$request->nisn,
            'nama'=>$request->nama,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'agama'=>$request->agama,
            'alamat'=>$request->alamat,
            'tempat_lahir'=>$request->tempat_lahir,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'status'=>$request->status,
            'foto'=>$request->foto,
            'tahun_ajaran'=>$request->tahun_ajaran,
            'kelas'=>$request->kelas,
            'jurusan'=>$request->jurusan,
            'user_id'=>$request->user_id
        ];
        DataSiswa::create($data);
        return response()->json([
            "message"=>"data tersimpan",
            "data"=>$data
        ]);
    }

    public function update(Request $request, $id){
        $data=[
            'nipd'=>$request->nipd,
            'nisn'=>$request->nisn,
            'nama'=>$request->nama,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'agama'=>$request->agama,
            'alamat'=>$request->alamat,
            'tempat_lahir'=>$request->tempat_lahir,
            'tanggal_lahir'=>$request->tanggal_lahir,
            'status'=>$request->status,
            'foto'=>$request->foto,
            'tahun_ajaran'=>$request->tahun_ajaran,
            'kelas'=>$request->kelas,
            'jurusan'=>$request->jurusan,
            'user_id'=>$request->user_id
        ];
        DataSiswa::find($id)->update($data);
        return response()->json([
            "message"=>"data berhasil diedit",
            "data"=>$data
        ]);
    }

    public function delete($id){
        DataSiswa::destroy($id);
        return response()->json([
            "message"=>"data berhasil dihapus"
        ]);
    }
}
