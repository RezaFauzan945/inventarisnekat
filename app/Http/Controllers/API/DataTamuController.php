<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DataTamu;

class DataTamuController extends Controller
{
    public function get() {
        $data = DataTamu::all();
        return response()->json([
            "massage" => "data berhasil dipangil",
            "data" => $data,
        ]);
    }
    public function getById($id) {
        $data = DataTamu::find($id);
        return response()->json([
            "massage" => "data berhasil didapat",
            "data" => $data,
        ]);
    }
    public function create(Request $request) {
        $data = DataTamu::create($request->all());
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data,
        ]);
    }
    public function update(Request $request, $id) {
        $data = DataTamu::find($id);
        $data->update($request->all());
        return response()->json([
            "massage" => "data berhasil diubah",
            "data" => $data,
        ]);
    }
    public function delete($id) {
        $data = DataTamu::find($id);
        $data->delete();
        return response()->json([
            "massage" => "data berhasil hapus",
        ]);
    }
}
