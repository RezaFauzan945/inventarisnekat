<?php

namespace App\Http\Controllers\API;

use App\Models\GuruMapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuruMapelController extends Controller
{
    public function get() {
        $data = GuruMapel::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = GuruMapel::find($id);
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'guru_id' => $request->guru_id,
            'mapel_id' => $request->mapel_id
        ];
        GuruMapel::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'guru_id' => $request->guru_id,
            'mapel_id' => $request->mapel_id
        ];
        $datagurumapel = GuruMapel::find($id);
        $datagurumapel->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        GuruMapel::destroy($id);
        return response()->json([
            "massage" => "data berhasil dihapus",
        ]);
    }
}
