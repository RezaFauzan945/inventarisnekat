<?php

namespace App\Http\Controllers\API;

use App\Models\GuruPensiun;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuruPensiunAPI extends Controller
{
    public function get() {
        $data = GuruPensiun::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = GuruPensiun::find($id);
        return response()->json([
            "massage" => "data per id terambil",
            "data" => $data
        ]);
    }


    public function create(Request $request) {
        $data = [
            'guru_id'=>$request->guru_id,
            'tahun_keluar'=>$request->tahun_keluar,
        ];
        GuruPensiun::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'guru_id'=>$request->guru_id,
            'tahun_keluar'=>$request->tahun_keluar,
        ];
        $datajadwal = GuruPensiun::find($id);
        $datajadwal->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        GuruPensiun::destroy($id);
        return response()->json([
            "massage" => "data berhasial dihapus",
        ]);
    }
}
