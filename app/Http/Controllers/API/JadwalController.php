<?php

namespace App\Http\Controllers\API;

use App\Models\DataJadwal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JadwalController extends Controller
{
    public function get() {
        $data = DataJadwal::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = DataJadwal::find($id);
        return response()->json([
            "massage" => "data per id terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'mapel_id' => $request->mapel_id,
            'jam' => $request->jam,
            'hari' => $request->hari,
            'kelas_id' => $request->kelas_id,
            'jurusan_id' => $request->jurusan_id,
            'guru_id' => $request->guru_id,
            'kode_semester' => $request->kode_semester,
            'status' => $request->status
        ];
        DataJadwal::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'mapel_id' => $request->mapel_id,
            'jam' => $request->jam,
            'hari' => $request->hari,
            'kelas_id' => $request->nama_kelas_id,
            'jurusan_id' => $request->jurusan_id,
            'guru_id' => $request->guru_id,
            'kode_semester' => $request->kode_semester,
            'status' => $request->status
        ];
        $datajadwal = DataJadwal::find($id);
        $datajadwal->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        DataJadwal::destroy($id);
        return response()->json([
            "massage" => "data berhasial dihapus",
        ]);
    }
}
