<?php

namespace App\Http\Controllers\API;

use App\Models\JamKehadiran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JamKehadiranController extends Controller
{
    public function get() {
        $data = JamKehadiran::all();
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = JamKehadiran::find($id);
        return response()->json([
            "massage" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'guru_id' => $request->guru_id,
            'tanggal_kehadiran' => $request->tanggal_kehadiran,
            'keterangan' => $request->keterangan
        ];
        JamKehadiran::create($data);
        return response()->json([
            "massage" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'guru_id' => $request->guru_id,
            'tanggal_kehadiran' => $request->tanggal_kehadiran,
            'keterangan' => $request->keterangan
        ];
        $jamkehadiran = JamKehadiran::find($id);
        $jamkehadiran->update($data);
        return response()->json([
            "massage" => "data berhasil terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        JamKehadiran::destroy($id);
        return response()->json([
            "massage" => "data berhasial dihapus",
        ]);
    }
}
