<?php
namespace App\Http\Controllers\API;

use App\Models\DataKelas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class KelasController extends BaseController
{
    public function get() {
        $data = DataKelas::all();
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = DataKelas::find($id);
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'nama_kelas'=>$request->nama_kelas
        ];
        DataKelas::create($data);
        return response()->json([
            "message" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'nama_kelas'=>$request->nama_kelas
        ];
        DataKelas::find($id)->update($data);
        return response()->json([
            "message" => "data terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        DataKelas::find($id)->delete();
        return response()->json([
            "message" => "data terhapus"
        ]);
    }
}
