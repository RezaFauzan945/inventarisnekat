<?php
namespace App\Http\Controllers\API;

use App\Models\Mapel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class MapelController extends BaseController
{
    public function get() {
        $data = Mapel::all();
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = Mapel::find($id);
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'nama_mapel'=>$request->nama_mapel,
            'kode_mapel'=>$request->kode_mapel
        ];
        Mapel::create($data);
        return response()->json([
            "message" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'nama_mapel'=>$request->nama_mapel,
            'kode_mapel'=>$request->kode_mapel
        ];
        Mapel::find($id)->update($data);
        return response()->json([
            "message" => "data terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        Mapel::find($id)->delete();
        return response()->json([
            "message" => "data terhapus",
        ]);
    }
}
