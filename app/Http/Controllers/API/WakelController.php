<?php

namespace App\Http\Controllers\API;

use App\Models\Wakel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class WakelController extends BaseController
{
    public function get() {
        $data = Wakel::all();
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function getById($id) {
        $data = Wakel::find($id);
        return response()->json([
            "message" => "data terambil",
            "data" => $data
        ]);
    }

    public function create(Request $request) {
        $data = [
            'guru_id'=>$request->guru_id,
            'kelas_id'=>$request->kelas_id,
            'jurusan_id'=>$request->jurusan_id,
            'tahun_ajaran'=>$request->tahun_ajaran,
            'status'=>$request->status
        ];
        Wakel::create($data);
        return response()->json([
            "message" => "data tersimpan",
            "data" => $data
        ]);
    }

    public function update(Request $request, $id) {
        $data = [
            'guru_id'=>$request->guru_id,
            'kelas_id'=>$request->kelas_id,
            'jurusan_id'=>$request->jurusan_id,
            'tahun_ajaran'=>$request->tahun_ajaran,
            'status'=>$request->status
        ];
        Wakel::find($id)->update($data);
        return response()->json([
            "message" => "data terupdate",
            "data" => $data
        ]);
    }

    public function delete($id) {
        Wakel::find($id)->delete();
        return response()->json([
            "message" => "data terhapus"
        ]);
    }
}
