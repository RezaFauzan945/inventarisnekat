<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Models\DataSiswa;
use App\Models\DataGuru;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        //catatan level
        //1 = admin
        //2 = siswa
        //3 = guru
        //4 = operator
        //5 = alumni
        $data = null;
        // if(!Auth::check()) {
        //     return redirect('/login');
        // }
        if(Auth::user()->level->level == 'Siswa' || Auth::user()->level->level == 'Alumni')
        {
            $data = DataSiswa::where('user_id', Auth::user()->id)->first();
        }
        else if(Auth::user()->level->level == 'Guru' || Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator')
        {
            $data = DataGuru::where('user_id', Auth::user()->id)->first();
        }
        return view('Dashboard',compact('data'));
    }

    public function getlogin()
    {
        return view('Login');
    }

    public function getsettings()
    {
        //catatan level
        //1 = admin
        //2 = siswa
        //3 = guru
        //4 = operator
        //5 = alumni
        $data = null;
        // if(!Auth::check()) {
        //     return redirect('/login');
        // }
        if(Auth::user()->level->level == 'Siswa' || Auth::user()->level->level == 'Alumni')
        {
            $data = DataSiswa::where('user_id', Auth::user()->id)->first();
        }
        else if(Auth::user()->level->level == 'Guru' || Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator')
        {
            $data = DataGuru::where('user_id', Auth::user()->id)->first();
        }
        return view('setting',compact('data'));
    }

}
