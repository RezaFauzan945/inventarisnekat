<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\DataSiswa;
use App\Models\DataGuru;
use App\Models\Barang;

use Illuminate\Http\Request;

class PagesControllerInventaris extends Controller
{
    public function index()
    {
        //catatan level
        //1 = admin
        //2 = siswa
        //3 = guru
        //4 = operator
        //5 = alumni
        $data = null;

        // if(!Auth::check()) {
        //     return redirect('/login');
        // }
        if (Auth::user()->level->level == 'Siswa' || Auth::user()->level->level == 'Alumni') {
            $data = DataSiswa::where('user_id', Auth::user()->id)->first();
        } else if (Auth::user()->level->level == 'Guru' || Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator') {
            $data = [
                'title' => 'Dashboard',
                'dataUser' => DataGuru::where('user_id', Auth::user()->id)->first(),
            ];
        }
        return view('/Modul/ModulInventaris/Dashboard', $data);
    }

    public function barang()
    {
        $data =
            [
                'title' => 'Barang',
                'data'  => Barang::all(),
            ];
        return view('/Modul/ModulInventaris/Barang', $data);
    }

    public function tambahBarang(Request $request)
    {
        $data = $request->all();

        Barang::create($data);
        return redirect('/inventaris/barang')->with('success', 'Data Barang Baru Sukses Ditambahkan');
    }

    public function barangGetUbah(Request $request)
    {
        return json_encode(Barang::firstWhere('id', $_POST['id']));
    }

    public function barangUbah(Request $request)
    {
        Barang::where('id', $_POST['id'])
        ->update([
            'nama' => $_POST['nama'],
            'jumlah' => $_POST['jumlah'],
            'spesifikasi' => $_POST['spesifikasi'],
            'sumber_dana' => $_POST['sumber_dana'],
            'id_ruangan' => $_POST['id_ruangan'],
            'keterangan' => $_POST['keterangan'],
            'kondisi' => $_POST['kondisi'],
            'jenis' => $_POST['jenis'],
        ]);
        return redirect('/inventaris/barang')->with('success', 'Data Barang Berhasil Diubah');
    }

    public function barangHapus(Request $request)
    {
        Barang::destroy($_POST['id']);
        return redirect('/inventaris/barang')->with('success', 'Data Barang Berhasil Dihapus');
    }

    public function barangMasuk()
    {
        $data =
            [
                'title' => 'Barang',
                'data'  => Barang::all(),
            ];
        return view('/Modul/ModulInventaris/BarangMasuk', $data);
    }

    public function barangKeluar()
    {
        $data =
            [
                'title' => 'Barang',
                'data'  => Barang::all(),
            ];
        return view('/Modul/ModulInventaris/BarangKeluar', $data);
    }
}
