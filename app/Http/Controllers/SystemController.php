<?php
namespace App\Http\Controllers;

use App\Models\DataGuru;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\DataSiswa;
class SystemController extends Controller
{
    public function index()
    {

        // Halaman Login
        // return view('login.index',[
        //     'title' => 'Login | SKULLI'
        // ]);
    }
    public function authenticate(Request $request)
    {
        // dd($request);
        // Jika Login Pakai Registrasi
        // $credentials = $request->validate([
        //     'email' => 'required|email',
        //     'password' => 'required'
        // ]);
        // if (Auth::attempt($credentials)){
        //     $request->session()->regenerate();
        //     return redirect()->intended('/dashboard');
        // }
        // Jika Login Tanpa Registrasi
        // $this->validate($request, [
        //     'email' => 'required|email',
        //     'password' => 'required'
        // ]);
        $user  = User::where(['email' => $request->email,
                            'password'=> $request->password])->first();

        // $datasiswa = DataSiswa::where(['user_id'=> $user->id])->first();
        // $datagtk = DataGuru::where(['user_id'=> $user->id])->first();
        // $userAuth = $datasiswa->User();
        // dd(Auth::login($userAuth));
        // if($datasiswa && $user->level == 'siswa'){
        //     Auth::login($userAuth);
        //     if(Auth::check()){
        //         return redirect('/');
        //     }
        // }else if($datagtk && $user->level == 'guru' || $user->level == 'admin' || $user->level == 'operator' || $user->level == 'tu'){
        //     Auth::login($datagtk);
        //     if(Auth::check()){
        //         return redirect('/');
        //     }
        // }else{
        //     return redirect('/login')->with('loginError','Email atau Password Salah');
        // }
        if($user)
        {
            Auth::login($user);
            if(Auth::check()){
                return redirect('/')->with('loginError','Berhasil Login');
            }
        }else{
            return redirect('/login')->with('loginError','Email atau Password Salah');
        }
    }

    public function updateprofile(Request $request)
    {
        if(Auth::user()->level->level == 'Siswa' || Auth::user()->level->level == 'Alumni')
        {
            //update data jika siswa
            $data = null;
            $siswa = DataSiswa::where('user_id',Auth::user()->id)->first();
            //jika request foto ada
            if($request->file('foto'))
            {
                //kondisi ekstensi yang valid
                if($request->file('foto')->getClientOriginalExtension() == "jpg" || $request->file('foto')->getClientOriginalExtension() == "jpeg" || $request->file('foto')->getClientOriginalExtension() == "png")
                {
                    $data = [
                        'nama'=>$request->nama,
                        'jenis_kelamin'=>$request->jenis_kelamin,
                        'agama'=>$request->agama,
                        'alamat'=>$request->alamat,
                        'tempat_lahir'=>$request->tempat_lahir,
                        'tanggal_lahir'=>$request->tanggal_lahir,
                        'foto'=>$request->file('foto')->store('public/images'),
                    ];
                //jika ekstensi tidak valid
                }else{
                    return redirect('/settings')->with('status','File harus berupa jpg, jpeg, atau png');
                }
            //jika tidak ada request foto
            }else{
                $data = [
                    'nama'=>$request->nama,
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'agama'=>$request->agama,
                    'alamat'=>$request->alamat,
                    'tempat_lahir'=>$request->tempat_lahir,
                    'tanggal_lahir'=>$request->tanggal_lahir,
                    'foto'=>$siswa->foto,
                ];
            }
            $query = DataSiswa::where('user_id',Auth::user()->id)->update($data);
            if($query){
                return redirect('/settings')->with('status','Data Berhasil Diupdate');
            }else{
                return redirect('/settings')->with('status','Data Gagal Diupdate');
            }
        }
        //update data guru
        else if(Auth::user()->level->level == 'Guru' || Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator')
        {
            $data = null;
            $guru = DataGuru::where('user_id',Auth::user()->id)->first();
            //jika ada request foto
            if($request->file('foto'))
            {
                //kondisi ektensi valid
                if($request->file('foto')->getClientOriginalExtension() == "jpg" || $request->file('foto')->getClientOriginalExtension() == "jpeg" || $request->file('foto')->getClientOriginalExtension() == "png")
                {
                    $data = [
                        'nama'=>$request->nama,
                        'jenis_kelamin'=>$request->jenis_kelamin,
                        'agama'=>$request->agama,
                        'alamat'=>$request->alamat,
                        'tempat_lahir'=>$request->tempat_lahir,
                        'tanggal_lahir'=>$request->tanggal_lahir,
                        'foto'=>$request->file('foto')->store('public/images'),
                    ];
                //jika ekstensi tidak valid
                }else{
                    return redirect('/settings')->with('status','File harus berupa jpg, jpeg, atau png');
                }
            //jika tidak ada request foto
            }else{
                $data = [
                    'nama'=>$request->nama,
                    'jenis_kelamin'=>$request->jenis_kelamin,
                    'agama'=>$request->agama,
                    'alamat'=>$request->alamat,
                    'tempat_lahir'=>$request->tempat_lahir,
                    'tanggal_lahir'=>$request->tanggal_lahir,
                    'foto'=>$guru->foto,
                ];
            }
            $query = DataGuru::where('user_id',Auth::user()->id)->update($data);
            if($query){
                return redirect('/settings')->with('status','Data Berhasil Diupdate');
            }else{
                return redirect('/settings')->with('status','Data Gagal Diupdate');
            }
        }
    }

    public function updateakun(Request $request)
    {
        if($request->password != null){
            if($request->password == Auth::user()->password){
                $data = [
                    'email'=>$request->email,
                    'password'=>$request->newpassword,
                ];
                $query = User::where('id',Auth::user()->id)->update($data);
                if($query){
                    return redirect('/settings')->with('status','Profile Berhasil DiUbah');
                }else{
                    return redirect('/settings')->with('status','Profile Gagal DiUbah');
                }
            }else{
                return redirect('/settings')->with('status','Password Lama Salah');
            }
        }else if($request->newpassword != null){
            return redirect('/settings')->with('status','Jika Ingin Mengganti Password,Isi Password Lama Terlebih Dahulu');
        }else{
            $data = [
                'email'=>$request->email,
            ];
            $query = User::where('id',Auth::user()->id)->update($data);
            if($query){
                return redirect('/settings')->with('status','Profile Berhasil DiUbah');
            }else{
                return redirect('/settings')->with('status','Profile Gagal DiUbah');
            }
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login')->with('loginError','Berhasil Logout');
    }
}
