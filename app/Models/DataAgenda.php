<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataAgenda extends Model
{
    protected $table = 'data_agenda';
    protected $fillable = ['nama_acara', 'penyelenggara', 'tempat', 'deskripsi', 'tanggal', 'due_date'];
    use HasFactory;
}
