<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataAlumni extends Model
{
    protected $table = 'data_alumni';
    protected $fillable = ['siswa_id','created_at','updated_at'];
    use HasFactory;

    public function Jurusan()
    {
        return $this->belongsTo(DataJurusan::class);
    }
}
