<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataGuru extends Model
{
    protected $table = 'data_guru';
    protected $fillable = [ 'nip','nama','jenis_kelamin','agama', 'alamat', 'tempat_lahir'
    , 'tanggal_lahir','foto','status_ptk','user_id'];
    use HasFactory;


    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
