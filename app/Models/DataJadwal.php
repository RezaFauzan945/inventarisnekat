<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataJadwal extends Model
{
    protected $table = 'data_jadwal';
    protected $fillable = ['nama_mapel_id', 'jam', 'hari', 'nama_kelas_id','nama_jurusan_id'
    , 'guru_id', 'kode_semester', 'status'];
    use HasFactory;

    public function Mapel()
    {
        //foreign key juga harus ditulis jika lebih dari 1 spasi(_)
        return $this->belongsTo(Mapel::class,'nama_mapel_id');
    }

    public function Kelas()
    {
        //foreign key juga harus ditulis jika lebih dari 1 spasi(_)
        return $this->belongsTo(DataKelas::class,'nama_kelas_id');
    }

    public function Jurusan()
    {
        //foreign key juga harus ditulis jika lebih dari 1 spasi(_)
        return $this->belongsTo(DataJurusan::class,'nama_jurusan_id');
    }

    public function Guru()
    {
        return $this->belongsTo(DataGuru::class);
    }


}
