<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataJurusan extends Model
{
    protected $table = 'data_jurusan';
    protected $fillable = ['nama_jurusan', 'keterangan', 'stand_year'];
    use HasFactory;
}
