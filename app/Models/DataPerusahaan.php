<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPerusahaan extends Model
{
    protected $table = 'data_perusahaan';
    protected $fillable = ['nama_perusahaan','alamat_perusahaan','created_at','updated_at'];
    use HasFactory;
}
