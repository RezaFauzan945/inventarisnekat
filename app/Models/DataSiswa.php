<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataSiswa extends Model
{
    protected $table = 'data_siswa';
    protected $fillable = ['nipd','nisn','nama','jenis_kelamin','agama','alamat','tempat_lahir'
    ,'tanggal_lahir','status','foto','tahun_ajaran','kelas','jurusan','user_id'];
    use HasFactory;

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Kelas()
    {
        //jika tidak ada penamaan id dalam foreign key,wajib mencantumkan nama foreign key dalam command
        //seperti dibawah ini
        return $this->belongsTo(DataKelas::class,'kelas');
    }

    public function Jurusan()
    {
        //jika tidak ada penamaan id dalam foreign key,wajib mencantumkan nama foreign key dalam command
        //seperti dibawah ini
        return $this->belongsTo(DataJurusan::class,'jurusan');
    }
}
