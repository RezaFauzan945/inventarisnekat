<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTamu extends Model
{
    protected $table = 'data_tamu';
    protected $fillable = ['nama_tamu','asal_tempat','tanggal_mengunjungi','keterangan','created_at','updated_at'];
    use HasFactory;
}
