<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuruMapel extends Model
{
    protected $table = 'guru_mapel';
    protected $fillable = ['guru_id', 'mapel_id'];
    use HasFactory;

    public function Guru()
    {
        return $this->belongsTo(DataGuru::class);
    }

    public function Mapel()
    {
        return $this->belongsTo(Mapel::class);
    }
}
