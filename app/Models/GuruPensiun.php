<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuruPensiun extends Model
{
    protected $table = 'guru_pensiun';
    protected $fillable = ['guru_id', 'tahun_keluar','created_at','updated_at'];
    use HasFactory;
}
