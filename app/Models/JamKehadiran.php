<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JamKehadiran extends Model
{
    protected $table = 'jam_kehadiran';
    protected $fillable = ['guru_id', 'tanggal_kehadiran', 'keterangan'];
    use HasFactory;

    public function Guru()
    {
        return $this->belongsTo(DataGuru::class);
    }
}
