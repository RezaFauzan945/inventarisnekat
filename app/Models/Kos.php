<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kos extends Model
{
    protected $fillable = ['jurusan','file','keterangan','created_at','updated_at'];
    use HasFactory;

    public function Jurusan()
    {
        return $this->belongsTo(DataJurusan::class,'jurusan');
    }
}
