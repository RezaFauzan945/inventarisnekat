<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mou extends Model
{
    protected $table = 'mou';
    protected $fillable = ['jurusan_id','perusahaan_id','file','keterangan','created_at','updated_at'];
    use HasFactory;

    public function Jurusan()
    {
        return $this->belongsTo(DataJurusan::class,'jurusan_id');
    }

    public function Perusahaan()
    {
        return $this->belongsTo(DataPerusahaan::class,'perusahaan_id');
    }
}
