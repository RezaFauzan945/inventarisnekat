<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wakel extends Model
{
    protected $table = 'wakel';
    protected $fillable = ['guru_id','kelas_id','jurusan_id','tahun_ajaran','status','created_at','updated_at'];
    use HasFactory;

    public function Guru()
    {
        return $this->belongsTo(DataGuru::class);
    }

    public function Kelas()
    {
        return $this->belongsTo(DataKelas::class,'kelas_id');
    }

    public function Jurusan()
    {
        return $this->belongsTo(DataJurusan::class,'jurusan_id');
    }
}
