<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mou', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('jurusan_id')->nullable(); //harus inisialisasi colom dlu
            $table->unsignedBigInteger('perusahaan_id')->nullable();//harus inisialisasi colom dlu
            $table->string('file');
            $table->text('keterangan');
            $table->timestamps();
        });
        Schema::table('mou', function (Blueprint $table) {
            $table->foreign('jurusan_id')
                    ->references('id')
                    ->on('data_jurusan')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('perusahaan_id')
                    ->references('id')
                    ->on('data_perusahaan')
                    ->onDelete('set null')
                    ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mou');
    }
}
