<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuruMapelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guru_mapel', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('guru_id')->nullable(); //harus inisialisasi colom dlu
            $table->unsignedBigInteger('mapel_id')->nullable(); //harus inisialisasi colom dlu
            $table->timestamps();
        });
        Schema::table('guru_mapel', function (Blueprint $table) {
            $table->foreign('guru_id')
                    ->on('data_guru')
                    ->references('id')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('mapel_id')
                    ->on('mapel')
                    ->references('id')
                    ->onDelete('set null')
                    ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guru_mapel');
    }
}
