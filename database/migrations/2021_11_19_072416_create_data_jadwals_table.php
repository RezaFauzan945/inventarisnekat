<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_jadwal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mapel_id')->nullable(); //harus inisialisasi colom dlu
            $table->time('jam');
            $table->unsignedBigInteger('hari')->nullable();
            $table->unsignedBigInteger('kelas_id')->nullable(); //harus inisialisasi colom dlu
            $table->unsignedBigInteger('jurusan_id')->nullable(); //harus inisialisasi colom dlu
            $table->unsignedBigInteger('guru_id')->nullable(); //harus inisialisasi colom dlu
            $table->integer('kode_semester');
            $table->boolean('status');
            $table->timestamps();
        });
        Schema::table('data_jadwal', function (Blueprint $table) {
            $table->foreign('mapel_id')
                    ->references('id')
                    ->on('mapel')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('kelas_id')
                    ->references('id')
                    ->on('data_kelas')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('jurusan_id')
                    ->references('id')
                    ->on('data_jurusan')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('guru_id')
                    ->references('id')
                    ->on('data_guru')
                    ->onDelete('set null')
                    ->onUpdate('set null');
            $table->foreign('hari')
                    ->references('id')
                    ->on('hari')
                    ->onDelete('set null')
                    ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_jadwal');
    }
}
