<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('spesifikasi');
            $table->foreignId('id_ruangan');
            $table->string('kondisi');
            $table->integer('jumlah');
            $table->string('sumber_dana');
            $table->string('jenis');
            $table->string('keterangan');
            $table->timestamps();
        });
        
        Schema::table('barang', function (Blueprint $table) {
            $table->foreign('id_ruangan')->references('id')->on('ruangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
