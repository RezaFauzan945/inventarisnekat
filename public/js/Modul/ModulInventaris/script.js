$(
  function()
{
 
  $('.tombolTambahData').on('click', function()
 {
   $('#modalLabel').html('Tambah Data Barang');
   $('.modal-footer button[type=submit]').html('Tambah Barang')

 }); 

 $('.tampilModalUbah').on('click', function()
 {

   $('#modalLabel').html('Ubah Data Barang');
   $('.modal-footer button[type=submit]').html('Ubah Data')
   $('.modal-body form').attr('action','/inventaris/barangubah')
   const id = $(this).data('id');
   $.ajax(
     {
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/inventaris/baranggetubah',
      data: {id : id},
      method: 'post',
      dataType: 'json',
      success: function(data){
        $('#nama').val(data.nama);
        $('#jumlah').val(data.jumlah);
        $('#spesifikasi').val(data.spesifikasi);
        $('#sumber_dana').val(data.sumber_dana);
        $('#id_ruangan').val(data.id_ruangan);
        $('#keterangan').val(data.keterangan);
        $('#kondisi').val(data.kondisi);
        $('#jenis').val(data.jenis);
        $('#id').val(data.id);
      }
     });

  }); 

}
);
