<?php
//potong nama yang panjang
$nama = explode(' ',$data->nama);
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <!-- CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/duotone.css"
        integrity="sha384-R3QzTxyukP03CMqKFe0ssp5wUvBPEyy9ZspCB+Y01fEjhMwcXixTyeot+S40+AjZ" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css"
        integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous" />
    <link rel="stylesheet" href="tes.css">
    <link rel="stylesheet" href="Style/Dashboard.css"> 

    <!-- {{-- Toast --}} -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">


    <title>Dashboard</title>
</head>

<body>
    <!-- navbar -->
    <nav id="nav" class="navbar navbar-light">
        <div class="container-fluid">
            <!-- <img src="asset/Img/logosimnekat-biru.PNG" alt=""> -->
            <img src="{{Asset('asset/Img/logosimnekat-biru.PNG')}}" alt="">
            <span class="navbar-brand mb-0 text-white">SIM NEKAT</span>
            <button onclick="sidebarOn()" class="toggle-navbar">
                <i class="fas fa-bars fa-lg"></i>
            </button>
        </div>
    </nav>
    <!-- navbar -->
    <nav id="nav2" class="navbar navbar-light">
        <div class="container-fluid">
            <!-- <img src="asset/Img/logosimnekat-biru.PNG" alt=""> -->
            <img src="{{Asset('asset/Img/logosimnekat-biru.PNG')}}" alt="">
            <span class="navbar-brand mb-0 text-white">SIM NEKAT</span>
            <button onclick="sidebarClose()" class="toggle-navbar">
                <i class="fas fa-bars fa-lg"></i>
            </button>
        </div>
    </nav>

    <!-- Sidebar -->
    <div class="sidebar" id="sidebar">

        <div class="sb-main-wrapper overflow-hidden" id="sb-main-wrapper">
            <!-- Search Wrapper -->
            <div class="search-wrapper hilangHD">

                <!-- Search Form -->
                <form action="GET" autocomplete="off">
                    <button class="btn search-btn"><i class="fas fa-search"></i></button>
                    <input class="form-control" placeholder="Cari Module..." type="text" name="search" id="search-inp">
                </form>
            </div>
            <!-- End Search Wrapper -->


            <!-- Sidebar Header -->
            <div class="sb-header pe-1 pt-3" id="sb-header">
                <img src="{{Asset('asset/Img/logosimnekat-biru.PNG')}}" alt="">
                <button onclick="sidebaroff()" class="toggle-sidebar float-end me-3"><i
                        class="fas fa-bars fa-lg"></i></button>
                <p>SIM NEKAT</p>
            </div>
            <!-- End Sb Header -->

            <!-- Main Sidebar -->
            <div class="sb-main" id="sb-main">
                <p class="hero-title" id="sb-title">Halaman</p>
                <!-- Wrapper Main Sidebar -->
                <div class="wrapper-main">
                    <!-- Child Main Sidebar -->
                    <div class="child-main">
                        <a href="">
                            <i class="fas fa-home fa-lg"></i>
                            <span>Beranda</span>
                        </a>
                    </div>
                    <div class="child-main">
                        <a href="">
                            <i class="fas fa-cog fa-lg"></i>
                            <span>Pengaturan</span>
                        </a>
                    </div>
                    <div class="child-main">
                        <a href="">
                            <i class="fas fa-info-circle fa-lg"></i>
                            <span>Tentang</span>
                        </a>
                    </div>
                    <!-- End Child Main Sidebar -->

                </div>
                <!-- End Wrapper Main Sidebar -->

            </div>
            <!-- End Main Sidebar -->

            <!-- Sidebar Profile -->
            <div class="sb-profile overflow-hidden" id="sb-profile">
                <a href="">
                    <img class="me-2" src="{{Asset('asset/Img/upin.jpg')}}" alt="">
                </a>
                <form action="/logout" method="POST">
                    @csrf
                    <button type="submit" class="exit-acc me-2"
                        style="background: none; text-decoration: none; border: none;">
                        <i class="fas fa-sign-out-alt fa-large"></i>
                    </button>
                </form>
                {{-- <div class="sb-pr-text-wrapper"> --}}

                @if ($data->nama != null)

                <div class="name overflow-hidden">{{ $data->nama }}</div>

                @endif

                <p class="job">{{Auth::user()->level->level}}</p>
                {{-- </div> --}}
            </div>
            <!-- End Sidebar Profile -->

            <div class="text-center hilangHD kopiright">@2021 Copyright SIM NEKAT</div>

        </div>

        <!-- Sidebar Mini -->
        <div class="sb-mini text-center" id="sb-mini">

            <!-- {{-- Sidebar Mini Header --}} -->
            <div class="sb-mini-header pt-4"><button onclick="sidebaron()"><i class="fas fa-bars fa-lg"></i></button>
            </div>

            <!-- {{-- Sidebar Mini Main --}} -->
            <div class="sb-mini-main pt-5">
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-home fa-lg"></i></a></div>
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-cog fa-lg"></i></a></div>
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-info-circle fa-lg"></i></a></div>
            </div>
            <form action="/logout" method="post">
                @csrf
                <!-- {{-- Sidebar Mini Profil --}} -->
                <div class="sb-mini-profil"><button type="submit"><i class="fas fa-sign-out-alt fa-lg"></i></button>
                </div>
            </form>
        </div>


    </div>

    <!-- Main Parent -->
    <div class="wrapper-parent ps-4" id="content">
        <div class="row">
            <div class="col-lg-6 pt-3">
                <!-- Search Wrapper -->
                <div class="search-wrapper">

                    <!-- Search Form -->
                    <form action="GET" autocomplete="off">
                        <button class="btn search-btn"><i class="fas fa-search"></i></button>
                        <input class="form-control" placeholder="Cari Module..." type="text" name="search"
                            id="search-inp">
                    </form>
                </div>
                <!-- End Search Wrapper -->
            </div>
            <div class="col-lg-6 pt-3">
                <!-- {{-- Minggu, 20 Juni 2077 --}} -->
                <p id="time_span" class="tanggal">A</p>
            </div>
        </div>

        <!-- Hero Title -->
        <div class="hero-title mt-4">
            <h1>Beranda</h1>
        </div>
        <!-- End Hero Title -->

        <!-- Wrapper Modul -->
        <div class="wrapper-modul">
            @if (Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator')
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Data PK</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Data.png')}}" class="modul-link" alt="">
                </div>
            </a>
            @endif
            @if (Auth::user()->level->level == 'Admin' || Auth::user()->level->level == 'Operator' || Auth::user()->level->level == 'Guru')
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Surat</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Surat.png')}}" class="modul-link" alt="">
                </div>
            </a>
            <a href="/inventaris">
                <div class="box-modul">
                    <p>Inventaris</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Inventaris.png')}}" class="modul-link" alt="">
                </div>
            </a>
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Perpustakaan</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Sysperpus.png')}}" class="modul-link" alt="">
                </div>
            </a>
            @endif
            @if (Auth::user()->Level->level == "Guru" || Auth::user()->Level->level == "Admin" || Auth::user()->Level->level == "Operator")
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Jadwal</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Jadwal.png')}}" class="modul-link" alt="">
                </div>
            </a>
            @endif
            @if (Auth::user()->Level->level == "Siswa" || Auth::user()->Level->level == "Alumni" || Auth::user()->Level->level == "Guru" || Auth::user()->Level->level == "Admin" || Auth::user()->Level->level == "Operator")
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Agenda</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Agenda.png')}}" class="modul-link" alt="">
                </div>
            </a>
            @endif
            @if (Auth::user()->Level->level == "Admin" || Auth::user()->Level->level == "Operator")
            <a href="http://youtube.com">
                <div class="box-modul">
                    <p>Keuangan</p>
                    <img src="{{Asset('asset/Img/dashboard modul/Keuangan.png')}}" class="modul-link" alt="">
                </div>
            </a>
            @endif
        </div>
    </div>
    <!-- End Wrapper Modul -->
    </div>
    <!-- End Wrapper Parent -->


    <!-- {{-- Javascript Live Clock --}} -->
    <script>
        timer();

        function timer() {
            var currentTime = new Date()
            var hours = currentTime.getHours()
            var minutes = currentTime.getMinutes()
            var sec = currentTime.getSeconds()
            if (minutes < 10) {
                minutes = "0" + minutes
            }
            if (sec < 10) {
                sec = "0" + sec
            }
            var t_str = hours + ":" + minutes + ":" + sec + " ";
            if (hours > 11) {
                t_str += "PM";
            } else {
                t_str += "AM";
            }

            //day
            const weekday = new Array(7);
            weekday[0] = "Minggu";
            weekday[1] = "Senin";
            weekday[2] = "Selasa";
            weekday[3] = "Rabu";
            weekday[4] = "Kamis";
            weekday[5] = "Jumat";
            weekday[6] = "Sabtu";

            const moon = new Array(12);
            moon[0] = "Januari";
            moon[1] = "Februari";
            moon[2] = "Maret";
            moon[3] = "April";
            moon[4] = "Mei";
            moon[5] = "Juni";
            moon[6] = "Juli";
            moon[7] = "Agustus";
            moon[8] = "September";
            moon[9] = "Oktober";
            moon[10] = "November";
            moon[11] = "Desember";
            var month = moon[currentTime.getMonth()];
            let day = weekday[currentTime.getDay()];
            document.getElementById('time_span').innerHTML = day + "," + " " + currentTime.getDate() + " " + month +
                " " +
                currentTime.getFullYear() + " | " + t_str;
            setTimeout(timer, 1000);
        }
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- {{-- Function Tombol Sidebar --}} -->
    <script>
        function sidebaron() {
            document.getElementById("sidebar").style.width = 225;
            document.getElementById("sb-main-wrapper").style.display = "block";
            document.getElementById("sb-mini").style.display = "none";
        }

        function sidebaroff() {
            document.getElementById("sidebar").style.width = 60;
            document.getElementById("sb-main-wrapper").style.display = "none";
            document.getElementById("sb-mini").style.display = "block";
        }

        // MOBILE SIDEBAR
        function sidebarClose() {
            document.getElementById("sidebar").style.display = "none";
            document.getElementById("nav2").style.display = "none";
            document.getElementById("nav").style.display = "block";
            document.getElementById("content").style.display = "block";
        }

        function sidebarOn() {
            document.getElementById("sidebar").style.display = "block";
            document.getElementById("nav").style.display = "none";
            document.getElementById("nav2").style.display = "block";
            document.getElementById("content").style.display = "none";
        }
    </script>

    <!-- {{-- Toast --}} -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <!-- {{-- Toastify --}} -->
    <!-- @if (Session::has('loginError')) -->
    <script>
        Toastify({
            text: "{{ Session::get('loginError') }}",
            className: "info",
            position: "left",
            style: {
                background: "#5956E9",
            }
        }).showToast();
    </script>
    <!-- @endif -->

</body>