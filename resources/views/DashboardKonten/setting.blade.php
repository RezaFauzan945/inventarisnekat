<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />

    <!-- CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/duotone.css"
        integrity="sha384-R3QzTxyukP03CMqKFe0ssp5wUvBPEyy9ZspCB+Y01fEjhMwcXixTyeot+S40+AjZ" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css"
        integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous" />
    <link rel="stylesheet" href="Style/setting.css">
    <!-- <link rel="stylesheet" href="{{ asset('Style/setting.css') }}"> -->

    <!-- {{-- Toast --}} -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">


    <title>Pengaturan</title>
</head>

<body>

    <!-- Sidebar -->
    <div class="sidebar" id="sidebar">

        <div class="sb-main-wrapper overflow-hidden" id="sb-main-wrapper">
            <div class="sb-top">
                <!-- Sidebar Header -->
                <div class="sb-header pe-1 pt-3" id="sb-header">
                    <a href="/" class="toggle-sidebar">
                        <i class="fas fa-arrow-left"></i>
                    </a>
                    <span>Pengaturan</span>
                </div>
                <!-- End Sb Header -->
            </div>

            <!-- Main Sidebar -->
            <div class="sb-main" id="sb-main">
                <span class="hero-title" id="sb-title">Pengaturan</span>
                <!-- Wrapper Main Sidebar -->
                <div class="wrapper-main">
                    <!-- Child Main Sidebar -->

                    <div class="child-main" id="p-umum"><a href="#" onclick="pUmum()"><span>Profil Umum</span></a></div>
                    <div class="child-main" id="p-akun"><a href="#" onclick="pAkun()"><span>Pengaturan Akun</span></a></div>

                    <!-- End Child Main Sidebar -->

                </div>
                <!-- End Wrapper Main Sidebar -->

            </div>
            <!-- End Main Sidebar -->

        </div>

        <!-- Sidebar Mini -->
        <div class="sb-mini text-center" id="sb-mini">

            <!-- {{-- Sidebar Mini Header --}} -->
            <div class="sb-mini-header pt-4"><button class="tombol" onclick="sidebaron()"><i class="fas fa-bars fa-lg"></i></button>
            </div>

            <!-- {{-- Sidebar Mini Main --}} -->
            <div class="sb-mini-main pt-5">
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-home fa-lg"></i></a></div>
                <div class="sb-mini-main-child"><a href=""><i class="far fa-user-circle fa-lg"></i></a></div>
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-cog fa-lg"></i></a></div>
                <div class="sb-mini-main-child"><a href=""><i class="fas fa-info-circle fa-lg"></i></a></div>
            </div>

        </div>


    </div>

    <!-- Main Parent -->
    <div class="wrapper-parent ps-4 pt-4" id="content">
        <div class="row">
            <div class="col-lg-6">
                <!-- Hero Title -->
                <div class="hero-title">
                    <h1 id="title" class="pengprof">Pengaturan Profil</h1>
                </div>
                <!-- End Hero Title -->
            </div>
            <div class="col-lg-6">
                <div class="tanggal">
                    <!-- {{-- Minggu, 20 Juni 2077 --}} -->
                    <p id="time_span"></p>
                </div>
            </div>
        </div>


        <!-- Wrapper Modul -->
        <div class="wrapper-child mt-4">
            <div class="p-profil" id="setting">
                <form action="/settings/profile" method="POST" enctype="multipart/form-data">
                    <div class="profile">
                        <h4>Profil</h4>
                        <p>Bagian ini berisi bagian profil yang dapat dilihat oleh publik</p>
                        <!-- Form -->
                        @csrf
                        <div class="nama">
                            <h5>Nama</h5>
                            <input class="input-content" name="nama" type="text" value="{{$data->nama}}">
                        </div>
                        @if (Auth::user()->level->level == 'Siswa' || Auth::user()->level->level == 'Alumni')
                        <div class="row mt-4">
                            <div class="kelas col-6">
                                <h5>Kelas</h5>
                                <input class="input-content beda" type="text" placeholder="{{$data->Kelas->nama_kelas}}"
                                    disabled>
                            </div>
                            <div class="jurusan col-6">
                                <h5>Jurusan</h5>
                                <input class="input-content beda" type="text"
                                    placeholder="{{$data->Jurusan->nama_jurusan}}" disabled>
                            </div>
                        </div>
                        @endif
                        {{-- khusus ganti gambar --}}
                        <div class="foto mt-4">
                            <div class="top">
                                <h5>Foto</h5>
                                <p>Foto yang diizinkan jpg, jpeg, dan png. Maksimal 10Mb.</p>
                            </div>
                            <div class="bot">
                                @if ($data->foto == null || $data->foto == 'null')
                                <img class="me-2" src="{{asset('asset/Img/guest.png')}}" alt="">
                                @else
                                <img class="me-2" src="{{asset('storage/'.$data->foto)}}" alt="">
                                @endif
                                <input type="file" name="foto" id="profil">
                                {{-- <button class="tombol">Ganti</button>
                                <button class="tombol" class="btn-2">Hapus</button> --}}
                            </div>
                        </div>
                    </div>
                    <div class="profile-pribadi">
                        <h4 class="profprib">Profil Pribadi</h4>
                        <p>Bagian ini berisi bagian profil yang hanya anda saja yang dapat melihat isi datanya</p>
                        <!-- Bagian 2 -->
                        <div class="row mb-4">
                            <div class="col-6">
                                <h5>Alamat</h5>
                                <input type="text" name="alamat" class="input-content" value="{{$data->alamat}}">
                            </div>
                            <div class="col-6">
                                <h5>Agama</h5>
                                <input type="text" name="agama" class="input-content" value="{{$data->agama}}">
                            </div>
                            <div class="col-6 mt-5">
                                <h5>Jenis Kelamin</h5>
                                <input type="text" name="jenis_kelamin" class="input-content"
                                    placeholder="laki-laki | perempuan" value="{{$data->jenis_kelamin}}">
                            </div>
                            <div class="col-6 mt-5">
                                <h5>Tempat Lahir</h5>
                                <input type="text" name="tempat_lahir" class="input-content"
                                    value="{{$data->tempat_lahir}}">
                            </div>
                            <div class="col-12 mt-5">
                                <h5>Tanggal Lahir</h5>
                                <input type="text" name="tanggal_lahir" class="input-content"
                                    value="{{$data->tanggal_lahir}}">
                            </div>
                        </div>
                        <p>Akun ini dibuat pada {{$data->created_at}}</p>
                    </div>
                    <div class="bot mt-5">
                        <button class="tombol" type="submit">Konfirmasi</button>
                        <a class="btn-2" href="/" style="margin-left: 35px">Kembali</a>
                    </div>
                </form>
                <!-- End Form -->
                <br><br><br>

            </div>

            <!-- Content2 -->
            <div class="p-akun" id="setting2">

                <!-- Form halaman 2 -->
                <form action="/settings/akun" method="POST">
                    @csrf
                    <div class="profile">
                        <h4>Pengaturan Akun</h4>
                        <p>Bagian ini berfungsi untuk mengubah pengaturan akun</p>
                        <p style="font-size: 13px;"><b>*jika ingin ganti email saja,jangan isi password</b></p>
                        <div class="row mt-4 mb-5">
                            <div class="nama col-12">
                                <h5>Email</h5>
                                <input class="input-content" name="email" type="text" value="{{Auth::user()->email}}">
                            </div>
                            <div class="kelas col-12 mt-4">
                                <h5>Password</h5>
                                <input class="input-content" name="password" type="password"
                                    placeholder="Masukan Password Sebelumnya...">
                            </div>
                            <div class="jurusan col-12 mt-4">
                                <h5>Konfirmasi Password</h5>
                                <input class="input-content" name="newpassword" type="password"
                                    placeholder="Masukan Password Baru...">
                            </div>
                        </div>
                        <button class="tombol">Ubah</button>
                        <a class="btn-2" href="/" style="margin-left: 35px">Kembali</a>
                        <br><br><br>
                    </div>
                </form>
            </div>

        </div>

    </div>

    <!-- {{-- Javascript Live Clock --}} -->
    <script>
        timer();

        function timer() {
            var currentTime = new Date()
            var hours = currentTime.getHours()
            var minutes = currentTime.getMinutes()
            var sec = currentTime.getSeconds()
            if (minutes < 10) {
                minutes = "0" + minutes
            }
            if (sec < 10) {
                sec = "0" + sec
            }
            var t_str = hours + ":" + minutes + ":" + sec + " ";
            if (hours > 11) {
                t_str += "PM";
            } else {
                t_str += "AM";
            }

            //day
            const weekday = new Array(7);
            weekday[0] = "Minggu";
            weekday[1] = "Senin";
            weekday[2] = "Selasa";
            weekday[3] = "Rabu";
            weekday[4] = "Kamis";
            weekday[5] = "Jumat";
            weekday[6] = "Sabtu";

            const moon = new Array(12);
            moon[0] = "Januari";
            moon[1] = "Februari";
            moon[2] = "Maret";
            moon[3] = "April";
            moon[4] = "Mei";
            moon[5] = "Juni";
            moon[6] = "Juli";
            moon[7] = "Agustus";
            moon[8] = "September";
            moon[9] = "Oktober";
            moon[10] = "November";
            moon[11] = "Desember";
            var month = moon[currentTime.getMonth()];
            let day = weekday[currentTime.getDay()];
            document.getElementById('time_span').innerHTML = day + "," + " " + currentTime.getDate() + " " + month +
                " " +
                currentTime.getFullYear() + " | " + t_str;
            setTimeout(timer, 1000);
        }
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- {{-- Function Tombol Sidebar --}} -->
    <script>
        function sidebaron() {
            document.getElementById("sidebar").style.width = 240;
            document.getElementById("sb-main-wrapper").style.display = "block";
            document.getElementById("sb-mini").style.display = "none";
        }

        function sidebaroff() {
            document.getElementById("sidebar").style.width = 60;
            document.getElementById("sb-main-wrapper").style.display = "none";
            document.getElementById("sb-mini").style.display = "block";
        }

        // MOBILE SIDEBAR
        function sidebarClose() {
            document.getElementById("sidebar").style.display = "none";
            document.getElementById("nav2").style.display = "none";
            document.getElementById("nav").style.display = "block";
            document.getElementById("content").style.display = "block";
        }

        function sidebarOn() {
            document.getElementById("sidebar").style.display = "block";
            document.getElementById("nav").style.display = "none";
            document.getElementById("nav2").style.display = "block";
            document.getElementById("content").style.display = "none";
        }

        function pUmum() {
            document.getElementById("p-umum").style.backgroundColor = "#3A37AC";
            document.getElementById("p-umum").style.borderLeft = "5px solid #120E86";
            document.getElementById("p-akun").style.backgroundColor = "#5956E9";
            document.getElementById("p-akun").style.borderLeft = "none";
            document.getElementById("setting").style.display = "block";
            document.getElementById("setting2").style.display = "none";
            document.getElementById("title").innerText = "Pengaturan Profil";
        }

        function pAkun() {
            document.getElementById("p-akun").style.backgroundColor = "#3A37AC";
            document.getElementById("p-akun").style.borderLeft = "5px solid #120E86";
            document.getElementById("p-umum").style.backgroundColor = "#5956E9";
            document.getElementById("p-umum").style.borderLeft = "none";
            document.getElementById("setting2").style.display = "block";
            document.getElementById("setting").style.display = "none";
            document.getElementById("title").innerText = "Akun";
        }
    </script>

    <!-- {{-- Toast --}} -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <!-- {{-- Toastify --}} -->
    @if (Session::has('status'))
    <script>
        Toastify({
            text: "{{ Session::get('status') }}",
            className: "info",
            position: "right",
            style: {
                background: "#5956E9",
            }
        }).showToast();
    </script>
    @endif

</body>