<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentang</title>


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />

    <!-- CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/duotone.css"
        integrity="sha384-R3QzTxyukP03CMqKFe0ssp5wUvBPEyy9ZspCB+Y01fEjhMwcXixTyeot+S40+AjZ" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css"
        integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('Style/about.css')}}">
</head>

<body>
    <div class="wrapper-master">

        <div class="sidebar">
            <div class="sb-header">
                <a href="" class="">
                    <i class="fas fa-arrow-left fa-lg"></i>
                </a>
                <p>Tentang</p>
            </div>
            <div class="sb-main-title">
                <p>Halaman Tentang</p>
            </div>

            <div class="sb-main-dua">
                <a href="#Sekolah">
                    <div class="wreppy">
                        <p>Tentang Sekolah</p>
                    </div>
                </a>
                <a href="#Nebeng">
                    <div class="wreppy">
                        <p>SIM NEKAT</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="sb-hidden"></div>

        <div class="wrapper-kanan">
            <h1 id="Sekolah">Tentang Sekolah</h1>
            <br>
            <h3>Sejarah Singkat</h3>
            <p>SMKN 1 Katapang berdiri Tahun 1999 dengan nama SMKN 4 Soreang. Unit Gedung Baru (UGB) SMKN 1 Katapang
                dibangun dari dana proyek LOAN OCF yang merupakan bantuan Pemerintah Jepang.</p>

            <p>Tahun Pelajaran 1999/2000 mulai menerima siswa baru untuk Program Keahlian Teknologi Penyempurnaan
                Tekstil, Teknik Elektro dan Mesin Perkakas (pendaftar ada 150 dan yang diterima 144 orang siswa), pada
                waktu itu KBM dilaksanakan di SMPN 1 Katapang.</p>

            <p>Tahun Pelajaran 2000/2001 SMKN 4 Soreang mulai menempati Unit Gedung Baru yang berlokasi di Jalan Ceuri
                Terusan Kopo KM 13,5 Desa Katapang, Kecamatan Katapang Kabupaten Bandung.</p>

            <p>Pada waktu peresmian UGB SMKN 1 Katapang yang dihadiri oleh pejabat dari Dinas Pendidikan Provinsi dan
                Kabupaten, Pemerintah setempat dan tokoh masyarakat setempat pada tahun 2000, disepakati nama SMKN 4
                Soreang menjadi SMKN 4 Katapang atas keinginan dari tokoh masyarakat bahwa nama SMKN 4 Soreang kurang
                sesuai karena berada di Kecamatan Katapang.
            </p>

            <p>Akhir Tahun 2000 keluar Surat Keputusan Ditpsmk Jakarta Nomor: 217/O/2000, tanggal 17 Nopember 2000,
                tentang pembukaan Sekolah yang menetapkan nama sekolah yang dulu bernama SMKN 4 Soreang secara resmi
                adalah SMKN 1 Katapang Kabupaten Bandung.
            </p>

            <p>Sejak tahun 1999 sampai sekarang siswa – siswa SMKN 1 Katapang banyak meraih prestasi terutama untuk
                perlombaan pramuka, baik tingkat Kabupaten Bandung maupun Provinsi Jawa Barat. Selain itu siswa - siswi
                SMKN 1 Katapang juga sering mewakili Kabupaten Bandung dalam LKS tingkat Provinsi dan Nasional.</p>

            <h3>Pendidikan</h3>

            <p>Lama Pendidikan di SMKN 1 Katapang 3 dan 4 Tahun. Program Pendidikan 3 Tahun untuk Kompetensi Keahlian
                Teknik Elektronika Industri (EIND), Teknik Pemesinan (TP), Teknik Gambar Mesin (TGM), Teknik Kendaraan
                Ringan (TKR), Teknik Komputer Jaringan (TKJ), Multimedia (MM) dan Rekayasa Perangkat Lunak (RPL).</p>

            <p>Dan Program Pendidikan 4 Tahun adalah untuk Kompetensi Keahlian Teknologi Penyempurnaan Tekstil (TEKS)
                dan Teknik Mekatronika (MKA).</p>

            <h3>Misi</h3>
            <ol>
                <li>
                    <p>Menerapkan manajemen pelayanan yang responsif dan akuntabel.</p>
                </li>
                <li>
                    <p>Melaksanakan Pembelajaran inovatif dan profesional yang didukung oleh teknologi berdasarkan pada
                        kearifan kebangsaan.
                    </p>
                </li>
                <li>
                    <p>Meningkatkan sumber daya manusia (SDM) yang sesuai dengan perkembangan zaman berdasarkan kearifan
                        kebangsaan.
                    </p>
                </li>
                <li>
                    <p>Menghasilkan lulusan yang berkarakter unggul, kompetitif sebagai pemimpin di masa depan.</p>
                </li>
                <li>
                    <p>Menyiapkan Lulusan yang peduli terhadap sesama manusia dan lingkungannya</p>
                </li>
            </ol>

            <h3>Visi</h3>

            <p>Sekolah Menengah Kejuruan Sebagai Pusat Penyiapan Generasi yang Unggul, Berkarakter Kebangsaan,
                Kompetitif dan Adaptabel.</p>

            <br><br>
            <!-- Tentang Sim Nekat -->
            <h1 id="Nebeng">Tentang SIM NEKAT</h1>
            <br>
            <h3>Apa itu SIM NEKAT</h3>
            <p>SIM NEKAT adalah sebuah aplikasi berbasis web yang dibuat oleh SMKN 1 Katapang guna untuk membuat segala
                urusan yang berhubungan dengan SMKN 1 Katapang lebih praktis dan mudah untuk dilakukan.</p>

            <h3>Fungsi SIM NEKAT</h3>
            <ol>
                <li>
                    <p>ha</p>
                </li>
                <li>
                    <p>ha</p>
                </li>
                <li>
                    <p>hi</p>
                </li>
                <li>
                    <p>hi</p>
                </li>
                <li>
                    <p>huu</p>
                </li>
            </ol>



        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>
