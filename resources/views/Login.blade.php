<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />

    <!-- CSS -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/duotone.css" integrity="sha384-R3QzTxyukP03CMqKFe0ssp5wUvBPEyy9ZspCB+Y01fEjhMwcXixTyeot+S40+AjZ" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/fontawesome.css" integrity="sha384-eHoocPgXsiuZh+Yy6+7DsKAerLXyJmu2Hadh4QYyt+8v86geixVYwFqUvMU8X90l" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('Style/StyleLogin.css')}}" />
    {{-- Toast --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <title>Login</title>
  </head>
  <body>
    <div class="content d-flex justify-content-between">
      <!-- Bagian Kiri -->
      <div class="bag-kiri d-flex flex-column justify-content-between overflow-hidden">
        <div class="hero-title d-flex align-items-center mt-5 justify-content-center">
          <img class="logo-smkn me-4 mt-3" src="{{asset('asset/Img/CEURI-ICO.png')}}" alt="Logo SMKN 1 Katapang" />
          <h1 class="title-smk text-light mt-3">SIM NEKAT</h1>
        </div>
        <div class="kiri-bawah align-items-end d-flex justify-content-between">
          <div class="contact">
            <ul>
              <li class="mb-4">
                <a href=""><i class="fal fa-phone-alt"></i></a>
              </li>
              <li class="mb-4">
                <a href=""><i class="fal fa-envelope"></i></a>
              </li>
              <li class="mb-5">
                <a href=""><i class="fab fa-instagram"></i></a>
              </li>
            </ul>
          </div>
          <div class="logo-simnekat">
            <img src="{{asset('asset/Img/logosimnekat-biru.PNG')}}" alt="" />
          </div>
        </div>
      </div>
      <!-- End Bagian Kiri -->

      <!-- Bagian Kanan/Form Login -->
      <div class="bag-kanan">
        <img class="smkn-mobile" src="asset/Img/Logo SMKN 1 Katapang.-edit.png" alt="" />
        <div class="card text-center card-luar">
          <div class="card-body d-flex justify-content-center align-items-center">
            <div class="card text-center card-dalam">
              <div class="card-body d-flex flex-column align-items-center">
                <h1 class="text-light title-masuk mb-4 mt-3">Masuk</h1>

                <!-- Form Login -->
                <form class="text-start" autocomplete="off" method="post">
                    @csrf
                  <!-- Form Email -->
                  <label for="Email" class="form-label text-white">Email</label>
                  <div class="input-group mb-4">
                    <span class="input-group-text bg-white border-0" id="basic-addon1"><i class="fal fa-envelope"></i></span>
                    <!-- Input Email -->
                    <input name="email" type="email" class="form-control border-0" placeholder="Masukan Email..." aria-label="Email" aria-describedby="basic-addon1" required />
                  </div>
                  <!-- End Form Email -->

                  <!-- Form Password -->
                  <label for="Password" class="form-label text-white">Sandi</label>
                  <div class="input-group mb-4">
                    <span class="input-group-text bg-white border-0" id="basic-addon2"><i class="fal fa-key"></i></span>
                    <!-- Input Password -->
                    <input name="password" type="password" class="form-control border-0" placeholder="Masukan Sandi..." aria-label="Password" aria-describedby="basic-addon2" required />
                  </div>
                  <!-- End Form Password -->

                  <!-- Submit button -->
                  <button type="submit" class="btn text-light w-50 mb-5">Masuk</button>
                </form>
                <!-- Form Login End -->

                <div class="contact-mobile w-75 d-flex justify-content-center">
                  <ul class="d-flex justify-content-between w-100">
                    <li>
                      <a href=""><i class="fal fa-phone-alt"></i></a>
                    </li>
                    <li>
                      <a href=""><i class="fal fa-envelope"></i></a>
                    </li>
                    <li>
                      <a href=""><i class="fab fa-instagram"></i></a>
                    </li>
                  </ul>
                </div>
                <blockquote class="w-75 text-white fst-italic mt-4">"Resiko Terbesar DiDunia Ini Adalah Tidak Mengambil Resiko Apapun"</blockquote>
                <blockquote class="text-white fst-italic">-Mark Zuckerberg</blockquote>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Bagian Kanan -->
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    {{-- Toast --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    {{-- Toastify --}}
    @if (Session::has('loginError'))
    <script>
        Toastify({
            text:"{{Session::get('loginError')}}",
            className : "info",
            position: "left",
            style:{
                background: "#5956E9",
            }
        }).showToast();
    </script>
    @endif
  </body>
</html>
