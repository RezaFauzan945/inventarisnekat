@extends('Modul.ModulInventaris.layouts.main')
@section('content')
    <!-- Menu Tables -->
    <div class="menu row ml-2 mt-3">
        <a class="col-lg-4 p-0" href="/inventaris/barang">
            <div class="menu-item active">
                Data Barang
            </div>
        </a>
        <a class="col-lg-4 p-0" href="/inventaris/barangmasuk">
            <div class="menu-item">
                Barang Masuk
            </div>
        </a>
        <a class="col-lg-4 p-0" href="/inventaris/barangkeluar">
            <div class="menu-item">
                Barang Keluar
            </div>
        </a>
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
    @endif
    <div class="card bayang mb-4">
        <!-- Button trigger modal -->
        <div class="card-header float-right">
            <button type="button" class="tombolTambahData float-end btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#modalTambahBarang">
                + Tambah Barang
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID</th>
                            <th>NAMA</th>
                            <th>ID RUANGAN</th>
                            <th>KONDISI</th>
                            <th>JUMLAH</th>
                            <th>SUMBER DANA</th>
                            <th>JENIS</th>
                            <th>KETERANGAN</th>
                            <th>OPSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach ($data as $dat)
                            <tr>
                                <th scope="row"><?= $i++; ?></th>
                                <td>{{ $dat['id'] }}</td>
                                <td>{{ $dat['nama'] }}</td>
                                <td><a href="/inventaris/detailruangan/{{ $dat['id_ruangan'] }}">{{ $dat['id_ruangan'] }}</a></td>
                                <td>{{ $dat['kondisi'] }}</td>
                                <td>{{ $dat['jumlah'] }}</td>
                                <td>{{ $dat['sumber_dana'] }}</td>
                                <td>{{ $dat['jenis'] }}</td>
                                <td>{{ $dat['keterangan'] }}</td>
                                <td>
                                    <a href="/inventaris/databarang/ubah/{{ $dat['id'] }}"
                                        class="fas fa-edit text-success tampilModalUbah" data-bs-toggle="modal"
                                        data-bs-target="#modalTambahBarang" data-id="{{ $dat['id'] }}"></a>
                                    
                                    <form action="/inventaris/baranghapus" class="d-inline" method="post">
                                        <input type="hidden" id="id" name="id" value="{{ $dat['id'] }}">
                                        @csrf
                                        <button class="btn-hapus border-0" onclick="return confirm('Apa Anda Yakin Hapus Data?')"><i class="fas fa-trash text-danger"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Tambah -->
    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Tambah Barang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/inventaris/tambahbarang" method="post">
                        @csrf
                        <input type="hidden" name='id' id="id">
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="nama" class="form-label">Nama Barang</label>
                                <input type="text" class="form-control" id="nama" required placeholder="Nama Barang" name="nama">
                            </div>

                            <div class="mb-3 col-6">
                                <label for="jumlah" class="form-label">Jumlah Barang</label>
                                <input type="number" class="form-control" id="jumlah" required placeholder="Jumlah Barang"
                                    name="jumlah">
                            </div>

                        </div>

                        <div class="row">

                            <div class="mb-3 col-6">
                                <label for="spesifikasi" class="form-label">Spesifikasi</label>
                                <input type="text" class="form-control" id="spesifikasi" required placeholder="Spesifikasi"
                                    name="spesifikasi">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="sumber_dana" class="form-label">Sumber Dana</label>
                                <input type="text" class="form-control" id="sumber_dana" required placeholder="Sumber Dana"
                                    name="sumber_dana">
                            </div>

                        </div>

                        <div class="row">

                            <div class="mb-3 col-6">
                                <label for="id_ruangan" class="form-label">Lokasi Barang</label>
                                <select class="form-select" aria-label="select-Jenis" required id="id_ruangan" name="id_ruangan">
                                    <option selected disabled value="">Ruangan</option>
                                    <option value="1">Ruang Tata Usaha</option>
                                    <option value="2">Ruang 1</option>
                                </select>
                            </div>
                            <div class="mb-3 col-6">
                                <label for="keterangan" class="form-label">Keterangan</label>
                                <input type="text" class="form-control" id="keterangan" required placeholder="Keterangan"
                                    name="keterangan">
                            </div>

                        </div>

                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="kondisi" class="form-label">Kondisi</label>
                                <select class="form-select" aria-label="select-kondisi" required id="kondisi" name="kondisi">
                                    <option selected disabled value="">Kondisi</option>
                                    <option value="Baik">Baik</option>
                                    <option value="Rusak">Rusak</option>
                                </select>
                            </div>

                            <div class="mb-3 col-6">
                                <label for="jenis" class="form-label">Jenis Barang</label>
                                <select class="form-select" aria-label="select-Jenis" required id="jenis" name="jenis">
                                    <option selected disabled value="">Jenis</option>
                                    <option value="Jenis 1">Jenis 1</option>
                                    <option value="Jenis 2">Jenis 2</option>
                                </select>
                            </div>

                        </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
