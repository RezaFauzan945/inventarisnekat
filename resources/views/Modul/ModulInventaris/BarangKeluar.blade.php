@extends('Modul.ModulInventaris.layouts.main')
@section('content')
    <!-- Menu Tables -->
    <div class="menu row ml-2 mt-3">
        <a class="col-lg-4 p-0" href="/inventaris/barang">
            <div class="menu-item">
                Data Barang
            </div>
        </a>
        <a class="col-lg-4 p-0" href="/inventaris/barangmasuk">
            <div class="menu-item">
                Barang Masuk
            </div>
        </a>
        <a class="col-lg-4 p-0" href="/inventaris/barangkeluar">
            <div class="menu-item active">
                Barang Keluar
            </div>
        </a>
    </div>

    <div class="card bayang mb-4">


        <!-- Button trigger modal -->
        <div class="card-header float-right">
            <button type="button" class="tombolTambahData float-end btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#modalTambahBarang">
                + Tambah Barang Masuk
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAMA</th>
                            <th>ID RUANGAN</th>
                            <th>KONDISI</th>
                            <th>JUMLAH</th>
                            <th>SUMBER DANA</th>
                            <th>JENIS</th>
                            <th>KETERANGAN</th>
                            <th>OPSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dat)
                    <tr>
                        <th scope="row">{{ $dat['id'] }}</th>
                        <td>{{ $dat['nama'] }}</td>
                        <td><a href="/inventaris/detailruangan/{{ $dat['id_ruangan'] }}">{{ $dat['id_ruangan'] }}</a></td>
                        <td>{{ $dat['kondisi'] }}</td>
                        <td>{{ $dat['jumlah'] }}</td>
                        <td>{{ $dat['sumber_dana'] }}</td>
                        <td>{{ $dat['jenis'] }}</td>
                        <td>{{ $dat['keterangan'] }}</td>
                        <td>
                            <a href="/databarang/ubah/{{ $dat['id_barang'] }}"
                                class="fas fa-edit text-success tampilModalUbah" data-bs-toggle="modal"
                                data-bs-target="#modalTambahBarang" data-id_barang="{{ $dat['id_barang'] }}"></a>
                            <a href="" class="fas fa-trash text-danger"></a>
                        </td>
                    </tr>
                @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Tambah -->
    <div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Tambah Barang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="mb-3 col-6">
                            <label for="nama_barang" class="form-label">Nama Barang</label>
                            <input type="text" class="form-control" id="nama_barang" placeholder="Nama Barang"
                                name="nama_barang">
                        </div>

                        <div class="mb-3 col-6">
                            <label for="jumlah_barang" class="form-label">Jumlah Barang</label>
                            <input type="number" class="form-control" id="jumlah_barang" placeholder="Jumlah Barang"
                                name="jumlah_barang">
                        </div>

                    </div>

                    <div class="row">

                        <div class="mb-3 col-6">
                            <label for="spesifikasi" class="form-label">Spesifikasi</label>
                            <input type="text" class="form-control" id="spesifikasi" placeholder="Spesifikasi"
                                name="spesifikasi">
                        </div>
                        <div class="mb-3 col-6">
                            <label for="sumber_dana" class="form-label">Sumber Dana</label>
                            <input type="text" class="form-control" id="sumber_dana" placeholder="Sumber Dana"
                                name="sumber_dana">
                        </div>

                    </div>

                    <div class="row">

                        <div class="mb-3 col-6">
                            <label for="lokasi_barang" class="form-label">Lokasi Barang</label>
                            <input type="text" class="form-control" id="lokasi_barang" placeholder="Lokasi Barang"
                                name="lokasi_barang">
                        </div>
                        <div class="mb-3 col-6">
                            <label for="keterangan" class="form-label">Keterangan</label>
                            <input type="text" class="form-control" id="keterangan" placeholder="Keterangan"
                                name="keterangan">
                        </div>

                    </div>

                    <div class="row">
                        <div class="mb-3 col-6">
                            <label for="kondisi" class="form-label">Kondisi</label>
                            <select class="form-select" aria-label="select-kondisi" id="kondisi" name="kondisi">
                                <option selected disabled>Kondisi</option>
                                <option value="Baik">Baik</option>
                                <option value="Rusak">Rusak</option>
                            </select>
                        </div>

                        <div class="mb-3 col-6">
                            <label for="jenis" class="form-label">Jenis Barang</label>
                            <select class="form-select" aria-label="select-Jenis" id="jenis" name="jenis">
                                <option selected disabled>Jenis</option>
                                <option value="Jenis 1">Jenis 1</option>
                                <option value="Jenis 2">Jenis 2</option>
                            </select>
                        </div>

                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </div>
        </div>
    </div>

@endsection

