@extends('Modul.ModulInventaris.layouts.main')
@section('content')
    
    <div class="container-fluid wrapper-modul align-items-center bg-dark">
        
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">Total Ruangan</div>
                <div class="row">
                    <div class="col-6">
                        <i class="fas fa-door-open" aria-hidden="true"></i>
                        <!-- <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/door-open.png') }}" alt=""> -->
                    </div>
                    <div class="col-6">
                        <span class="link_name">256</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">Total Barang Masuk</div>
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/barangM.svg') }}" alt="" />
                    </div>
                    <div class="col-6">
                        <span class="link_name">156</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">Total Barang Keluar</div>
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/barangK.svg') }}" alt="" />
                    </div>
                    <div class="col-6">
                        <span class="link_name">150</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">Total Barang Rusak</div>
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/broken.svg') }}" alt="" />
                    </div>
                    <div class="col-6 total">
                        <span class="link_name">56</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">Total Barang Hilang</div>
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/barangHlng.svg') }}" alt="" />
                    </div>
                    <div class="col-6 total">
                        <span class="link_name">100</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
        <a href="#">
            <div class="box-modul">
                <div class="modul_name">xsd</div>
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('asset/Modul/ModulInventaris/img/drawable/barangM.svg') }}" alt="" />
                    </div>
                    <div class="col-6">
                        <span class="link_name">256</span>
                    </div>
                </div>
                <div class="detail">
                    <i class="fas fa-info-circle"></i>
                    <span>Lihat Detail</span>
                </div>
            </div>
        </a>
    </div>

@endsection
