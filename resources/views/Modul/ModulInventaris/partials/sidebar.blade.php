<!-- Sidebar -->
<div class="sidebar-nekat">
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <div class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon">
                <a href="/inventaris" class="text-light">
                    <h6>Inventaris</h6>
                    <sup>Nekat</sup>
                </a>
            </div>
            <div class="toggled-nekat">
                <!-- Sidebar Toggler (Sidebar) -->
                <div class="">
                    <button id="sidebarToggleTop" class="btn rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- <i class="fa fa-bars" id="sidebarToggle"></i> -->
                </div>
            </div>
            <div class="sidebar-brand-text text-white mx-3">Inventaris<sup>Nekat</sup></div>
        </div>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="/inventaris">
                <i class="fa fa-home"></i>
                <span class="link_name">Beranda</span>
            </a>
            <span class="tooltip">Beranda</span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <img class="imgWhite" src="{{ asset('asset/Modul/ModulInventaris/img/drawable/peminjaman.svg') }}" alt="" class="imgWhite">
                <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                <span class="link_name">Peminjaman</span>
            </a>
            <span class="tooltip">Peminjaman</span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/inventaris/barang">
                <i class="fas fa-archive"></i>
                <span class="link_name">Barang</span>
            </a>
            <span class="tooltip">Barang</span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <i class="fas fa-truck"></i>
                <span class="link_name">Data Suplier</span>
            </a>
            <span class="tooltip">Data Suplier</span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <i class="fas fa-file-alt"></i>
                <span class="link_name">Laporan</span>
            </a>
            <span class="tooltip">Laporan</span>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <div class="profile_content mt-auto">
            <div class="profile">
            <div class="profile_details">
                <!-- profile Img -->
                <img src="{{ asset('asset/Modul/ModulInventaris/img/user1.jpg') }}" alt="">
                <div class="name_job">
                <div class="name">Ulz</div>
                <div class="job">Web Designer</div>
                </div>
                <form action="/logout" method="post">
                    @csrf
                    <button type="submit" class="logout-button"><i class="fa fa-sign-out-alt" id="log-out" aria-hidden="true"></i></button>
                </form>
            </div>
            </div>
        </div>
    </ul>
    
</div>
<!-- End of Sidebar -->