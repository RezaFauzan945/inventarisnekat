<!-- Topbar -->
<nav class="navbar navbar-expand topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn float-start btn-link d-md-none rounded-circle mr-3 text-light">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Navbar -->
    <h1 class="text-center mx-auto text-light">
        Inventaris Nekat
    </h1>

</nav>
<!-- End of Topbar -->
