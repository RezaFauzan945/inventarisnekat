<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AgendaController;
use App\Http\Controllers\API\AlumniController;
use App\Http\Controllers\API\DataGuruController;
use App\Http\Controllers\API\GuruMapelController;
use App\Http\Controllers\API\JadwalController;
use App\Http\Controllers\API\JamKehadiranController;
use App\Http\Controllers\API\DataSiswaController;
use App\Http\Controllers\API\MapelController;
use App\Http\Controllers\API\KelasController;
use App\Http\Controllers\API\WakelController;
use App\Http\Controllers\API\DataKosController;
use App\Http\Controllers\API\DataMouController;
use App\Http\Controllers\API\DataPerusahaanController;
use App\Http\Controllers\API\DataTamuController;
use App\Http\Controllers\API\GuruPensiunAPI;
use App\Http\Controllers\API\JurusanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//lulus testing
Route::get('/agenda', [AgendaController::class, 'get']);
Route::get('/agenda/{id}', [AgendaController::class, 'getById']);
Route::post('/agenda', [AgendaController::class, 'create']);
Route::put('/agenda/{id}', [AgendaController::class, 'update']);
Route::delete('/agenda/{id}', [AgendaController::class, 'delete']);

//lulus testing
Route::get('/alumni', [AlumniController::class, 'get']);
Route::get('/alumni/{id}', [AlumniController::class, 'getById']);
Route::post('/alumni', [AlumniController::class, 'create']);
Route::put('/alumni/{id}', [AlumniController::class, 'update']);
Route::delete('/alumni/{id}', [AlumniController::class, 'delete']);

//lulus testing
Route::get('/jadwal', [JadwalController::class, 'get']);
Route::get('/jadwal/{id}', [JadwalController::class, 'getById']);
Route::post('/jadwal', [JadwalController::class, 'create']);
Route::put('/jadwal/{id}', [JadwalController::class, 'update']);
Route::delete('/jadwal/{id}', [JadwalController::class, 'delete']);

//lulus tesing
Route::get('/jamkehadiran', [JamKehadiranController::class, 'get']);
Route::get('/jamkehadiran/{id}', [JamKehadiranController::class, 'getById']);
Route::post('/jamkehadiran', [JamKehadiranController::class, 'create']);
Route::put('/jamkehadiran/{id}', [JamKehadiranController::class, 'update']);
Route::delete('/jamkehadiran/{id}', [JamKehadiranController::class, 'delete']);

//lulus testing
Route::get('/gurumapel', [GuruMapelController::class, 'get']);
Route::get('/gurumapel/{id}', [GuruMapelController::class, 'getById']);
Route::post('/gurumapel', [GuruMapelController::class, 'create']);
Route::put('/gurumapel/{id}', [GuruMapelController::class, 'update']);
Route::delete('/gurumapel/{id}', [GuruMapelController::class, 'delete']);

//lulus testing
Route::get('/guru',[DataGuruController::class, 'get']);
Route::get('/guru/{id}',[DataGuruController::class, 'getById']);
Route::post('/guru',[DataGuruController::class, 'create']);
Route::put('/guru/{id}',[DataGuruController::class, 'update']);
Route::delete('/guru/{id}',[DataGuruController::class, 'delete']);

//lulus testing
Route::get('/siswa',[DataSiswaController::class, 'get']);
Route::get('/siswa/{id}',[DataSiswaController::class, 'getById']);
Route::post('/siswa',[DataSiswaController::class, 'create']);
Route::put('/siswa/{id}',[DataSiswaController::class, 'update']);
Route::delete('/siswa/{id}',[DataSiswaController::class, 'delete']);

//lulus Testing
Route::get('/mapel',[MapelController::class, 'get']);
Route::get('/mapel/{id}',[MapelController::class, 'getById']);
Route::post('/mapel',[MapelController::class, 'create']);
Route::put('/mapel/{id}',[MapelController::class, 'update']);
Route::delete('/mapel/{id}',[MapelController::class, 'delete']);

//lulus testing
Route::get('/kelas',[KelasController::class, 'get']);
Route::get('/kelas/{id}',[KelasController::class, 'getById']);
Route::post('/kelas',[KelasController::class, 'create']);
Route::put('/kelas/{id}',[KelasController::class, 'update']);
Route::delete('/kelas/{id}',[KelasController::class, 'delete']);

//lulus testing
Route::get('/wakel',[WakelController::class, 'get']);
Route::get('/wakel/{id}',[WakelController::class, 'getById']);
Route::post('/wakel',[WakelController::class, 'create']);
Route::put('/wakel/{id}',[WakelController::class, 'update']);
Route::delete('/wakel/{id}',[WakelController::class, 'delete']);

//lulus testing
Route::get('/kos',  [DataKosController::class, 'get']);
Route::get('/kos/{id}',  [DataKosController::class, 'getById']);
Route::post('/kos',  [DataKosController::class, 'create']);
Route::put('/kos/{id}',  [DataKosController::class, 'update']);
Route::delete('/kos/{id}',  [DataKosController::class, 'delete']);

//lulus testing
Route::get('/mou',  [DataMouController::class, 'get']);
Route::get('/mou/{id}',  [DataMouController::class, 'getById']);
Route::post('/mou',  [DataMouController::class, 'create']);
Route::put('/mou/{id}',  [DataMouController::class, 'update']);
Route::delete('/mou/{id}',  [DataMouController::class, 'delete']);

//lulus testing
Route::get('/perusahaan',  [DataPerusahaanController::class, 'get']);
Route::get('/perusahaan/{id}',  [DataPerusahaanController::class, 'getById']);
Route::post('/perusahaan',  [DataPerusahaanController::class, 'create']);
Route::put('/perusahaan/{id}',  [DataPerusahaanController::class, 'update']);
Route::delete('/perusahaan/{id}',  [DataPerusahaanController::class, 'delete']);

//lulus testing
Route::get('/tamu',  [DataTamuController::class, 'get']);
Route::get('/tamu/{id}',  [DataTamuController::class, 'getById']);
Route::post('/tamu',  [DataTamuController::class, 'create']);
Route::put('/tamu/{id}',  [DataTamuController::class, 'update']);
Route::delete('/tamu/{id}',  [DataTamuController::class, 'delete']);

//lulus Testing
Route::get('/jurusan',  [JurusanController::class, 'get']);
Route::get('/jurusan/{id}',  [JurusanController::class, 'getById']);
Route::post('/jurusan',  [JurusanController::class, 'create']);
Route::put('/jurusan/{id}',  [JurusanController::class, 'update']);
Route::delete('/jurusan/{id}',  [JurusanController::class, 'delete']);

//lulus testing
Route::get('/gurupensiun',  [GuruPensiunAPI::class, 'get']);
Route::get('/gurupensiun/{id}',  [GuruPensiunAPI::class, 'getById']);
Route::post('/gurupensiun',  [GuruPensiunAPI::class, 'create']);
Route::put('/gurupensiun/{id}',  [GuruPensiunAPI::class, 'update']);
Route::delete('/gurupensiun/{id}',  [GuruPensiunAPI::class, 'delete']);
