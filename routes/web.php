<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@index')->middleware('auth');
Route::get('/login','PagesController@getlogin')->middleware('guest')->name('login');
Route::get('/settings','PagesController@getsettings')->middleware('auth');

Route::post('/login','SystemController@authenticate')->middleware('guest');
Route::post('/logout','SystemController@logout')->middleware('auth');
Route::post('/settings/profile','SystemController@updateprofile')->middleware('auth');
Route::post('/settings/akun','SystemController@updateakun')->middleware('auth');


//route inventaris
Route::get('/inventaris','PagesControllerInventaris@index')->middleware('auth');

Route::get('/inventaris/barang','PagesControllerInventaris@barang')->middleware('auth');
Route::post('/inventaris/tambahbarang','PagesControllerInventaris@tambahBarang')->middleware('auth');
Route::post('/inventaris/baranggetubah','PagesControllerInventaris@barangGetUbah')->middleware('auth');
Route::post('/inventaris/barangubah','PagesControllerInventaris@barangUbah')->middleware('auth');
Route::post('/inventaris/baranghapus','PagesControllerInventaris@barangHapus')->middleware('auth');

Route::get('/inventaris/barangmasuk','PagesControllerInventaris@barangMasuk')->middleware('auth');
Route::get('/inventaris/barangkeluar','PagesControllerInventaris@barangKeluar')->middleware('auth');
